import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "template"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    "com.h2database" % "h2" % "1.3.167",
    "com.typesafe.slick" %% "slick" % "1.0.0",
    "org.webjars" % "webjars-play" % "2.1-RC1",
    "org.webjars" % "bootstrap" % "2.1.1",
    "org.webjars" % "angularjs" % "1.0.3",
    "org.webjars" % "angular-ui" % "0.3.2-1",
    "org.webjars" % "angular-strap" % "0.6.3",
    "org.webjars" % "jasmine" % "1.3.1",
    "org.webjars" % "select2" % "3.2",
    "org.webjars" % "font-awesome" % "3.0.0",
    "org.webjars" % "dojo" % "1.8.3",
    "javax.jcr" % "jcr" % "2.0",
    "org.modeshape" % "modeshape-jcr" % "3.2.0.Final",
    "org.infinispan" % "infinispan-cachestore-jdbc" % "5.2.3.Final",
    "c3p0" % "c3p0" % "0.9.1.1",
    "com.jayway.restassured" % "rest-assured" % "1.8.0" % "test"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += "JBoss Public Repository" at "http://repository.jboss.org/nexus/content/groups/public/"
  )

}
