import com.jayway.restassured.RestAssured
import com.jayway.restassured.RestAssured.expect
import javax.jcr.query.Row
import org.specs2.mutable.Specification
import play.api.test.TestServer
import store.JCR
import play.api.test.Helpers._
import org.hamcrest.Matchers.equalToIgnoringCase

class JcrSpec extends Specification {
  val jcr = new JCR()
  val testPort = 3333
  RestAssured.port = testPort

  "Repository" should {
    "create session" in {
      jcr.login() must not be null
    }

    "create child in root" in {
      jcr.inSession({session => {
        session.getRootNode().addNode("test")
      }})

      val newNode = jcr.query("/jcr:root/test", Some((row: Row) => {
      }), "XPath")

      newNode must not be null
      newNode.getRows.getSize() must beEqualTo(1)
    }

    "provide tree as json" in {
      running(TestServer(testPort)) {
        expect().statusCode(200).content(equalToIgnoringCase("gallo")).when().get("tree")
      }
      success
    }

  }
}
