package models
import play.api.Play.current

object AppDB extends BaseDB {

  lazy val database = getDb
  lazy val dal = getDal

}