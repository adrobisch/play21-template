package store

import org.modeshape.jcr.{ModeShapeEngine, RepositoryConfiguration}
import javax.jcr.ItemNotFoundException
import javax.jcr.query.{QueryManager, Row, QueryResult}
import scala.collection.JavaConversions._

class JCR(repositoryConfig : String = "repository.json") {
    val config: RepositoryConfiguration = RepositoryConfiguration.read(repositoryConfig)

    if (config.validate().hasErrors()) {
      //Logger.error("Problems starting the Modeshape engine:");
      //Logger.error(config.validate().toString());
    }

    val engine: ModeShapeEngine = new ModeShapeEngine()
    engine.start();
    val repository: javax.jcr.Repository = engine.deploy(config);

    def inSession(block: javax.jcr.Session => Unit) = {
      val session = repository.login()
      try {
        block(session)
      } finally {
        session.save()
        session.logout()
      }
    }

    def login(): javax.jcr.Session = {
      repository.login()
    }

    def getNode(session : javax.jcr.Session, id: String): Option[javax.jcr.Node] = {
      try {
        Option(session.getNodeByIdentifier(id))
      } catch {
        case e: ItemNotFoundException => {
          None
        } // todo: handle error
      }
    }

    def query(query: String, rowFunction : Option[Row => Unit], lang : String = "JCR-SQL2") : QueryResult = {
      var result : QueryResult = null
      inSession(session => {
        val mgr: QueryManager = session.getWorkspace().getQueryManager()
        result = mgr.createQuery(query, lang).execute()

        for (row <-result.getRows) {
          rowFunction.getOrElse((row:Row) => {}).apply(row.asInstanceOf[Row])
        }

      })
      result
    }
}
